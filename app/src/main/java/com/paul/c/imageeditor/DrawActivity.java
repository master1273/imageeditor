package com.paul.c.imageeditor;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.os.Bundle;
import android.util.FloatMath;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;


import it.moondroid.colormixer.HSLFragment;

/**
 * Created by master on 17.11.2015.
 */
public class DrawActivity extends Activity implements HSLFragment.OnColorChangeListener{


    private DrawingView drawView;

    private ImageButton colorPickerBtn;
    private ImageButton invertBtn;
    private ImageButton undoBtn;
    private ImageButton shareBtn;
    private ImageButton zoomIn;
    private ImageButton zoomOut;
    private ImageButton paintBtn;

    private ImageView image;

    private float downx = 0;
    private float downy = 0;
    private float upx = 0;
    private float upy = 0;

    private Bitmap bmp;
    private Bitmap alteredBitmap;
    private Canvas canvas;
    private Paint paint;
    private Matrix matrix = new Matrix();
    private Matrix paintMatrix = new Matrix();
    private Matrix savedMatrix = new Matrix();

    // We can be in one of these 3 states
    private static final int NONE = 0;
    private static final int DRAG = 1;
    private static final int ZOOM = 2;
    private int mode = NONE;

    // Remember some things for zooming
    private PointF start = new PointF();
    private PointF mid = new PointF();
    private float oldDist = 1f;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.draw);

        Display currentDisplay = getWindowManager().getDefaultDisplay();
        float dw = currentDisplay.getWidth();
        float dh = currentDisplay.getHeight();

        RectF drawableRect = new RectF(0, 0, dw, dh);
        RectF viewRect = new RectF(0, 0, dw, dh);
        matrix.setRectToRect(drawableRect, viewRect, Matrix.ScaleToFit.CENTER);

        image = (ImageView) findViewById(R.id.pic_background);

        image.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if (paintBtn.isSelected()) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            downx = event.getX();
                            downy = event.getY();
                            break;
                        case MotionEvent.ACTION_MOVE:
                            upx = event.getX();
                            upy = event.getY();
                            canvas.drawLine(downx, downy, upx, upy, paint);
                            image.invalidate();
                            downx = upx;
                            downy = upy;
                            break;
                        case MotionEvent.ACTION_UP:
                            upx = event.getX();
                            upy = event.getY();
                            canvas.drawLine(downx, downy, upx, upy, paint);
                            image.invalidate();
                            break;
                        case MotionEvent.ACTION_CANCEL:
                            break;
                        default:
                            break;
                    }
                } else {
                    ImageView v = (ImageView) view;
                    dumpEvent(event);

                    // Handle touch events here...
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_DOWN:
                            savedMatrix.set(matrix);
                            start.set(event.getX(), event.getY());
                            mode = DRAG;
                            break;
                        case MotionEvent.ACTION_POINTER_DOWN:
                            oldDist = spacing(event);
                            if (oldDist > 10f) {
                                savedMatrix.set(matrix);
                                midPoint(mid, event);
                                mode = ZOOM;
                            }
                            break;
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_POINTER_UP:
                            mode = NONE;
                            break;
                        case MotionEvent.ACTION_MOVE:
                            if (mode == DRAG) {
                                // ...
                                matrix.set(savedMatrix);
                                matrix.postTranslate(event.getX() - start.x, event.getY() - start.y);
                            } else if (mode == ZOOM) {
                                float newDist = spacing(event);
                                if (newDist > 10f) {
                                    matrix.set(savedMatrix);
                                    float scale = newDist / oldDist;
                                    matrix.postScale(scale, scale, mid.x, mid.y);
                                }
                            }
                            break;
                    }
                    v.setImageMatrix(matrix);
                }
                return true;
            }
        });

        bmp = BitmapFactory.decodeByteArray(
                getIntent().getByteArrayExtra("byteArray"), 0, getIntent().getByteArrayExtra("byteArray").length);

        alteredBitmap = Bitmap.createBitmap((int)dw, (int)dh, bmp.getConfig());/*bmp.getWidth(), bmp
                .getHeight(), bmp.getConfig());*/
        canvas = new Canvas(alteredBitmap);
        paint = new Paint();
        paint.setColor(MyApp.getColorRes(R.color.primary));
        paint.setStrokeWidth(5);
        canvas.drawBitmap(bmp, matrix, paint);



        image.setImageBitmap(alteredBitmap);

        colorPickerBtn = (ImageButton) findViewById(R.id.picker_btn);
        colorPickerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HSLFragment colorPicker = HSLFragment.newInstance(paint.getColor());
                colorPicker.show(getFragmentManager(), "dialog");
            }
        });

        invertBtn = (ImageButton) findViewById(R.id.invert_btn);
        invertBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                float[] colorTransform = {
                        0, 1f, 0, 0, 0,
                        0, 0, 0f, 0, 0,
                        0, 0, 0, 0f, 0,
                        0, 0, 0, 1f, 0};

                ColorMatrix colorMatrix = new ColorMatrix();
                colorMatrix.setSaturation(0f); //Remove Colour
                colorMatrix.set(colorTransform); //Apply the Red

                ColorMatrixColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
                Paint paint = new Paint();
                paint.setColorFilter(colorFilter);

                Bitmap resultBitmap = Bitmap.createBitmap(bmp, 0, bmp.getHeight(), bmp.getWidth(), bmp.getHeight());

                canvas.drawBitmap(resultBitmap, 0, 0, paint);

                image.invalidate();
            }
        });

        undoBtn = (ImageButton) findViewById(R.id.undo_btn);
        shareBtn = (ImageButton) findViewById(R.id.share_btn);
        zoomIn = (ImageButton) findViewById(R.id.zoom_in_btn);
        zoomOut = (ImageButton) findViewById(R.id.zoom_out_btn);

        paintBtn = (ImageButton) findViewById(R.id.paint_btn);
        paintBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View button) {
                button.setSelected(!button.isSelected());
            }
        });

        image.invalidate();

    }

    @Override
    public void onColorChange(int color) {

    }

    @Override
    public void onColorConfirmed(int color) {
        paint.setColor(color);
    }

    @Override
    public void onColorCancel() {

    }

    /** Show an event in the LogCat view, for debugging */
    private void dumpEvent(MotionEvent event) {
        String names[] = { "DOWN", "UP", "MOVE", "CANCEL", "OUTSIDE",
                "POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?" };
        StringBuilder sb = new StringBuilder();
        int action = event.getAction();
        int actionCode = action & MotionEvent.ACTION_MASK;
        sb.append("event ACTION_").append(names[actionCode]);
        if (actionCode == MotionEvent.ACTION_POINTER_DOWN
                || actionCode == MotionEvent.ACTION_POINTER_UP) {
            sb.append("(pid ").append(
                    action >> MotionEvent.ACTION_POINTER_ID_SHIFT);
            sb.append(")");
        }
        sb.append("[");
        for (int i = 0; i < event.getPointerCount(); i++) {
            sb.append("#").append(i);
            sb.append("(pid ").append(event.getPointerId(i));
            sb.append(")=").append((int) event.getX(i));
            sb.append(",").append((int) event.getY(i));
            if (i + 1 < event.getPointerCount())
                sb.append(";");
        }
        sb.append("]");
    }

    /** Determine the space between the first two fingers */
    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float)Math.sqrt(x * x + y * y);
    }

    /** Calculate the mid point of the first two fingers */
    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }
}
