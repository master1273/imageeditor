package com.paul.c.imageeditor;

/**
 * Created by maste on 17.11.2015.
 */
public class Constants {
    public static int REQUEST_CAMERA = 1;
    public static int SELECT_FILE = 2;
    public static int POST_DELAY_FOR_EXIT = 2000;
}
